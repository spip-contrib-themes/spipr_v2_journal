<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4journal
// Langue: fr
// Date: 16-04-2020 18:19:50
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4journal_description' => 'Crisp like a new sheet of paper',
	'theme_bs4journal_slogan' => 'Crisp like a new sheet of paper',
);
?>